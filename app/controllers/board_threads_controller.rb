class BoardThreadsController < ApplicationController

  prepend_before_action :set_board_thread, only: %i[ show edit update destroy ]
  prepend_before_action :authenticate_user!, only: [:edit, :destroy, :update, :new, :create]
  append_before_action :authorize_user!

  # GET /board_threads or /board_threads.json
  def index
    @board_threads = BoardThread.all
  end

  # GET /board_threads/1 or /board_threads/1.json
  def show
  end

  # GET /board_threads/new
  def new
    @board_thread = BoardThread.new
  end

  # GET /board_threads/1/edit
  def edit
  end

  # POST /board_threads or /board_threads.json
  def create
    @board_thread = BoardThread.new(board_thread_params)

    respond_to do |format|
      if @board_thread.save
        format.html { redirect_to board_thread_url(@board_thread), notice: "Board thread was successfully created." }
        format.json { render :show, status: :created, location: @board_thread }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @board_thread.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /board_threads/1 or /board_threads/1.json
  def update
    respond_to do |format|
      if @board_thread.update(board_thread_params)
        format.html { redirect_to board_thread_url(@board_thread), notice: "Board thread was successfully updated." }
        format.json { render :show, status: :ok, location: @board_thread }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @board_thread.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /board_threads/1 or /board_threads/1.json
  def destroy
    @board_thread.destroy

    respond_to do |format|
      format.html { redirect_to board_threads_url, notice: "Board thread was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

    def authorize_user!
      if !@board_thread.is_restricted
        return
      end
      if !user_signed_in?
        return authenticate_user!
      end
      if !current_user.has_permission
        redirect_to root_path, alert: "You have no permission to this thread."
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_board_thread
      @board_thread = BoardThread.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def board_thread_params
      params.require(:board_thread).permit(:id, :name, :description, :board_id, :is_restricted)
    end
end
