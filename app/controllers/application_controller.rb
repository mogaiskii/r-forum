class ApplicationController < ActionController::Base
  include Authentication

  http_basic_authenticate_with :name => "admin", :password => "lovesgachi", if: :admin_controller?

  def admin_controller?
    self.class < ActiveAdmin::BaseController
  end
end
