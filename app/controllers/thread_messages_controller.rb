class ThreadMessagesController < ApplicationController
  prepend_before_action :set_thread_message, only: %i[ show edit update destroy ]
  prepend_before_action :authenticate_user!, only: [:edit, :destroy, :update, :new, :create]
  append_before_action :authorize_user!

  # GET /thread_messages or /thread_messages.json
  def index
    @thread_messages = ThreadMessage.all
  end

  # GET /thread_messages/1 or /thread_messages/1.json
  def show
  end

  # GET /thread_messages/new
  def new
    @thread_message = ThreadMessage.new
    @board = Board.find(params[:board_id])
    @board_thread = BoardThread.find(params[:board_thread_id])
  end

  # GET /thread_messages/1/edit
  def edit
  end

  # POST /thread_messages or /thread_messages.json
  def create
    puts "\n\n\nat least here\n\n"

    @board = Board.find(params[:board_id])
    @board_thread = BoardThread.find(params[:board_thread_id])

    @thread_message = ThreadMessage.new(thread_message_params)
    @thread_message.user = current_user
    @thread_message.board_thread = @board_thread

    respond_to do |format|
      if @thread_message.save
        format.html { redirect_to board_board_thread_url(@board, @board_thread), notice: "Thread message was successfully created." }
        format.json { render :show, status: :created, location: @thread_message }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @thread_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /thread_messages/1 or /thread_messages/1.json
  def update
    respond_to do |format|
      if @thread_message.update(thread_message_params)
        format.html { redirect_to thread_message_url(@thread_message), notice: "Thread message was successfully updated." }
        format.json { render :show, status: :ok, location: @thread_message }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @thread_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /thread_messages/1 or /thread_messages/1.json
  def destroy
    @thread_message.destroy

    respond_to do |format|
      format.html { redirect_to thread_messages_url, notice: "Thread message was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thread_message
      @thread_message = ThreadMessage.find(params[:id])
      @board = Board.find(params[:board_id])
      @board_thread = BoardThread.find(params[:board_thread_id])
    end

    # Only allow a list of trusted parameters through.
    def thread_message_params
      params.require(:thread_message).permit(:user_id, :board_thread_id, :message, :created_at, :updated_at, :image)
    end

    def authorize_user!
      if @thread_message.present?
        if !@thread_message.board_thread.is_restricted
          return
        end
      else
        board_thread = BoardThread.find(params[:board_thread_id])
        if !board_thread.is_restricted
          return
        end
      end
      if !user_signed_in?
        return authenticate_user!
      end
      if !current_user.has_permission
        redirect_to root_path, alert: "You have no permission to this thread."
      end
    end
end
