class PersonalMessagesController < ApplicationController
  before_action :set_personal_message, only: %i[ show edit update destroy ]
  before_action :authenticate_user!

  # GET /personal_messages or /personal_messages.json
  def index
    @personal_messages = PersonalMessage.all
  end

  # GET /personal_messages/1 or /personal_messages/1.json
  def show
  end

  # GET /personal_messages/new
  def new
    @personal_message = PersonalMessage.new
  end

  # GET /personal_messages/1/edit
  def edit
  end

  # POST /personal_messages or /personal_messages.json
  def create
    @personal_message = PersonalMessage.new(personal_message_params)

    respond_to do |format|
      if @personal_message.save
        format.html { redirect_to personal_message_url(@personal_message), notice: "Personal message was successfully created." }
        format.json { render :show, status: :created, location: @personal_message }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @personal_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personal_messages/1 or /personal_messages/1.json
  def update
    respond_to do |format|
      if @personal_message.update(personal_message_params)
        format.html { redirect_to personal_message_url(@personal_message), notice: "Personal message was successfully updated." }
        format.json { render :show, status: :ok, location: @personal_message }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @personal_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /personal_messages/1 or /personal_messages/1.json
  def destroy
    @personal_message.destroy

    respond_to do |format|
      format.html { redirect_to personal_messages_url, notice: "Personal message was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_personal_message
      @personal_message = PersonalMessage.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def personal_message_params
      params.require(:personal_message).permit(:user_from_id, :user_to_id, :message, :created_at)
    end
end
