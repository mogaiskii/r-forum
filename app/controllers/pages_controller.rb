class PagesController < ApplicationController
  def index
    @boards = Board.includes(:board_threads).all
    @user = current_user
  end
end