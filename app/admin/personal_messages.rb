ActiveAdmin.register PersonalMessage do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :user_from_id, :user_to_id, :message, :was_seen
  #
  # or
  #
  # permit_params do
  #   permitted = [:user_from_id, :user_to_id, :message, :was_seen]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
