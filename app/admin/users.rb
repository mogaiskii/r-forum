ActiveAdmin.register User do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :username, :password, :avatar_url, :signature, :last_online, :has_permission
  form do |f|
    f.input :username
    f.input :password
    f.input :signature
    f.input :last_online
    f.input :has_permission
    f.actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:username, :password_digest, :avatar_url, :signature, :last_online, :has_permission]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
