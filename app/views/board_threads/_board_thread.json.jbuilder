json.extract! board_thread, :id, :id, :name, :description, :forum_id, :is_restricted, :created_at, :updated_at
json.url board_thread_url(board_thread, format: :json)
