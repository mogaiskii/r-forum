json.extract! personal_message, :id, :user_from_id, :user_to_id, :message, :created_at, :created_at, :updated_at
json.url personal_message_url(personal_message, format: :json)
