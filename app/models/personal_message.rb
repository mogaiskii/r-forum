class PersonalMessage < ApplicationRecord
  belongs_to :user_from, :class_name => 'User', :foreign_key => 'user_from_id', inverse_of: :outcoming_messages
  belongs_to :user_to, :class_name => 'User', :foreign_key => 'user_to_id', inverse_of: :incoming_messages

  default_scope { order(created_at: :desc) }
  scope :messages_of, lambda { |user|
    where(user_from: user.id).or(user_to: user.id).order(created_at: :desc)
  }

  scope :messages_received_by, lambda { |user|
    where(user_to: user.id)
  }
  def set_seen_by_dialog(user_to, user_from)
    by_receiver.update_all(was_seen: true)
  end

  def was_seen?
    return was_seen
  end

end
