class ThreadMessage < ApplicationRecord
  belongs_to :user
  belongs_to :board_thread
  has_one_attached :image
  
  default_scope { order(:created_at) }
end
