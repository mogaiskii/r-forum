class User < ApplicationRecord
    has_secure_password

    default_scope { order(created_at: :desc) }

    scope :correspondents_of, lambda { |user|
        joins(:outcoming_messages).where(outcoming_messages: {user_to: user}).order(created_at: desc)
    }

    has_many :board_thread
end
