class Board < ApplicationRecord
  default_scope { order(:created_at) }

  has_many :board_threads
end
