class BoardThread < ApplicationRecord
  belongs_to :board
  belongs_to :user
  has_many :thread_messages

  default_scope { order(created_at: :desc) }

  def is_restricted?
    is_restricted
  end
end
