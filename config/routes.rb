Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  
  resources :boards, only: [] do
    resources :board_threads, only: [:show, :new, :create] do
      resources :thread_messages, only: [:new, :create]
    end
  end

  resources :users
  resources :personal_messages

  post "login", to: "sessions#create"
  delete "logout", to: "sessions#destroy"
  get "login", to: "sessions#new"

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "pages#index"
end
