require "test_helper"

class BoardThreadsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @board_thread = board_threads(:one)
  end

  test "should get index" do
    get board_threads_url
    assert_response :success
  end

  test "should get new" do
    get new_board_thread_url
    assert_response :success
  end

  test "should create board_thread" do
    assert_difference("BoardThread.count") do
      post board_threads_url, params: { board_thread: { description: @board_thread.description, forum_id: @board_thread.forum_id, id: @board_thread.id, is_restricted: @board_thread.is_restricted, name: @board_thread.name } }
    end

    assert_redirected_to board_thread_url(BoardThread.last)
  end

  test "should show board_thread" do
    get board_thread_url(@board_thread)
    assert_response :success
  end

  test "should get edit" do
    get edit_board_thread_url(@board_thread)
    assert_response :success
  end

  test "should update board_thread" do
    patch board_thread_url(@board_thread), params: { board_thread: { description: @board_thread.description, forum_id: @board_thread.forum_id, id: @board_thread.id, is_restricted: @board_thread.is_restricted, name: @board_thread.name } }
    assert_redirected_to board_thread_url(@board_thread)
  end

  test "should destroy board_thread" do
    assert_difference("BoardThread.count", -1) do
      delete board_thread_url(@board_thread)
    end

    assert_redirected_to board_threads_url
  end
end
