require "test_helper"

class PersonalMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @personal_message = personal_messages(:one)
  end

  test "should get index" do
    get personal_messages_url
    assert_response :success
  end

  test "should get new" do
    get new_personal_message_url
    assert_response :success
  end

  test "should create personal_message" do
    assert_difference("PersonalMessage.count") do
      post personal_messages_url, params: { personal_message: { created_at: @personal_message.created_at, message: @personal_message.message, user_from_id: @personal_message.user_from_id, user_to_id: @personal_message.user_to_id } }
    end

    assert_redirected_to personal_message_url(PersonalMessage.last)
  end

  test "should show personal_message" do
    get personal_message_url(@personal_message)
    assert_response :success
  end

  test "should get edit" do
    get edit_personal_message_url(@personal_message)
    assert_response :success
  end

  test "should update personal_message" do
    patch personal_message_url(@personal_message), params: { personal_message: { created_at: @personal_message.created_at, message: @personal_message.message, user_from_id: @personal_message.user_from_id, user_to_id: @personal_message.user_to_id } }
    assert_redirected_to personal_message_url(@personal_message)
  end

  test "should destroy personal_message" do
    assert_difference("PersonalMessage.count", -1) do
      delete personal_message_url(@personal_message)
    end

    assert_redirected_to personal_messages_url
  end
end
