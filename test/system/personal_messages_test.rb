require "application_system_test_case"

class PersonalMessagesTest < ApplicationSystemTestCase
  setup do
    @personal_message = personal_messages(:one)
  end

  test "visiting the index" do
    visit personal_messages_url
    assert_selector "h1", text: "Personal messages"
  end

  test "should create personal message" do
    visit personal_messages_url
    click_on "New personal message"

    fill_in "Created at", with: @personal_message.created_at
    fill_in "Message", with: @personal_message.message
    fill_in "User from", with: @personal_message.user_from_id
    fill_in "User to", with: @personal_message.user_to_id
    click_on "Create Personal message"

    assert_text "Personal message was successfully created"
    click_on "Back"
  end

  test "should update Personal message" do
    visit personal_message_url(@personal_message)
    click_on "Edit this personal message", match: :first

    fill_in "Created at", with: @personal_message.created_at
    fill_in "Message", with: @personal_message.message
    fill_in "User from", with: @personal_message.user_from_id
    fill_in "User to", with: @personal_message.user_to_id
    click_on "Update Personal message"

    assert_text "Personal message was successfully updated"
    click_on "Back"
  end

  test "should destroy Personal message" do
    visit personal_message_url(@personal_message)
    click_on "Destroy this personal message", match: :first

    assert_text "Personal message was successfully destroyed"
  end
end
