require "application_system_test_case"

class BoardThreadsTest < ApplicationSystemTestCase
  setup do
    @board_thread = board_threads(:one)
  end

  test "visiting the index" do
    visit board_threads_url
    assert_selector "h1", text: "Board threads"
  end

  test "should create board thread" do
    visit board_threads_url
    click_on "New board thread"

    fill_in "Description", with: @board_thread.description
    fill_in "Forum", with: @board_thread.forum_id
    fill_in "Id", with: @board_thread.id
    check "Is restricted" if @board_thread.is_restricted
    fill_in "Name", with: @board_thread.name
    click_on "Create Board thread"

    assert_text "Board thread was successfully created"
    click_on "Back"
  end

  test "should update Board thread" do
    visit board_thread_url(@board_thread)
    click_on "Edit this board thread", match: :first

    fill_in "Description", with: @board_thread.description
    fill_in "Forum", with: @board_thread.forum_id
    fill_in "Id", with: @board_thread.id
    check "Is restricted" if @board_thread.is_restricted
    fill_in "Name", with: @board_thread.name
    click_on "Update Board thread"

    assert_text "Board thread was successfully updated"
    click_on "Back"
  end

  test "should destroy Board thread" do
    visit board_thread_url(@board_thread)
    click_on "Destroy this board thread", match: :first

    assert_text "Board thread was successfully destroyed"
  end
end
