require "application_system_test_case"

class ThreadMessagesTest < ApplicationSystemTestCase
  setup do
    @thread_message = thread_messages(:one)
  end

  test "visiting the index" do
    visit thread_messages_url
    assert_selector "h1", text: "Thread messages"
  end

  test "should create thread message" do
    visit thread_messages_url
    click_on "New thread message"

    fill_in "Created at", with: @thread_message.created_at
    fill_in "Message", with: @thread_message.message
    fill_in "Thread", with: @thread_message.thread_id
    fill_in "Updated at", with: @thread_message.updated_at
    fill_in "User", with: @thread_message.user_id
    click_on "Create Thread message"

    assert_text "Thread message was successfully created"
    click_on "Back"
  end

  test "should update Thread message" do
    visit thread_message_url(@thread_message)
    click_on "Edit this thread message", match: :first

    fill_in "Created at", with: @thread_message.created_at
    fill_in "Message", with: @thread_message.message
    fill_in "Thread", with: @thread_message.thread_id
    fill_in "Updated at", with: @thread_message.updated_at
    fill_in "User", with: @thread_message.user_id
    click_on "Update Thread message"

    assert_text "Thread message was successfully updated"
    click_on "Back"
  end

  test "should destroy Thread message" do
    visit thread_message_url(@thread_message)
    click_on "Destroy this thread message", match: :first

    assert_text "Thread message was successfully destroyed"
  end
end
