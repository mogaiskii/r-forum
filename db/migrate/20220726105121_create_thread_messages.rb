class CreateThreadMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :thread_messages do |t|
      t.references :user
      t.references :board_thread
      t.string :message

      t.timestamps
    end

    add_foreign_key :thread_messages, :users, column: :user_id, on_delete: :cascade
    add_foreign_key :thread_messages, :board_threads, column: :board_thread_id, on_delete: :cascade
  end
end
