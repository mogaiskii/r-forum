class CreateBoardThreads < ActiveRecord::Migration[7.0]
  def change
    create_table :board_threads do |t|
      t.string :name
      t.string :description
      t.references :board, null: false
      t.references :user, null: true
      t.boolean :is_restricted, default: false

      t.timestamps
    end

    add_foreign_key :board_threads, :boards, column: :board_id, on_delete: :cascade
    add_foreign_key :board_threads, :users, column: :user_id, on_delete: :nullify
  end
end
