class CreatePersonalMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :personal_messages do |t|
      t.references :user_from, foreign_key: {to_table: :users}
      t.references :user_to, foreign_key: {to_table: :users}
      t.string :message
      t.boolean :was_seen, default: false

      t.timestamps
    end

    add_foreign_key :personal_messages, :users, column: :user_from_id, on_delete: :cascade
    add_foreign_key :personal_messages, :users, column: :user_to_id, on_delete: :cascade
  end
end
