class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :username, unique: true
      t.string :password_digest
      t.string :avatar_url, null: true
      t.string :signature, null: true
      t.timestamp :last_online, null: true
      t.boolean :has_permission, default: false

      t.timestamps
    end
  end
end
