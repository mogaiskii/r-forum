# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_07_27_060238) do
  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.integer "resource_id"
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "board_threads", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "board_id", null: false
    t.integer "user_id"
    t.boolean "is_restricted", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board_id"], name: "index_board_threads_on_board_id"
    t.index ["user_id"], name: "index_board_threads_on_user_id"
  end

  create_table "boards", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personal_messages", force: :cascade do |t|
    t.integer "user_from_id"
    t.integer "user_to_id"
    t.string "message"
    t.boolean "was_seen", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_from_id"], name: "index_personal_messages_on_user_from_id"
    t.index ["user_to_id"], name: "index_personal_messages_on_user_to_id"
  end

  create_table "thread_messages", force: :cascade do |t|
    t.integer "user_id"
    t.integer "board_thread_id"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board_thread_id"], name: "index_thread_messages_on_board_thread_id"
    t.index ["user_id"], name: "index_thread_messages_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "password_digest"
    t.string "avatar_url"
    t.string "signature"
    t.datetime "last_online", precision: nil
    t.boolean "has_permission", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "board_threads", "boards", on_delete: :cascade
  add_foreign_key "board_threads", "users", on_delete: :nullify
  add_foreign_key "personal_messages", "users", column: "user_from_id"
  add_foreign_key "personal_messages", "users", column: "user_from_id", on_delete: :cascade
  add_foreign_key "personal_messages", "users", column: "user_to_id"
  add_foreign_key "personal_messages", "users", column: "user_to_id", on_delete: :cascade
  add_foreign_key "thread_messages", "board_threads", on_delete: :cascade
  add_foreign_key "thread_messages", "users", on_delete: :cascade
end
